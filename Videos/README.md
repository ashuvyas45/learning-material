# SOFT SKILL IMPROVEMENT VIDEO LIST

1. [How to achieve ambitions goal my marginal adjustment in your life, by: Stephen Duneier](https://www.youtube.com/watch?v=TQMbvJNRpLE&list=LL&index=1)
2. [The psychology of self-motivation, by: Scott Geller](https://www.youtube.com/watch?v=7sxpKhIbr0E&list=LL&index=2)
    - Feeling competent doing worthwhile thing is sign of self-motivation and empowerment.
    - Three important questions to know if someone feels empowered: Can you do it? Will it work? Is it worthwhile? 
